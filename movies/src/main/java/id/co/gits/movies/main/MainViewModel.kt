package id.co.gits.movies.main

import android.app.Application
import id.co.gits.gitsbase.BaseViewModel
import id.co.gits.gitsutils.SingleLiveEvent
import id.co.gits.gitsutils.data.model.Movie
import id.co.gits.gitsutils.data.source.GitsDataSource

/**
 * Created by irfanirawansukirman on 26/01/18.
 */
class MainViewModel(context: Application) : BaseViewModel(context) {

    var movieListLive = SingleLiveEvent<List<Movie>>()
    val snackBarMessageRemote = SingleLiveEvent<String>()

    override fun start() {
        super.start()

    }

    override fun onClearDisposable() {
        super.onClearDisposable()
        mRepository.onClearDisposables()
    }


    private fun getMovies() {
        mRepository.getMovies(object : GitsDataSource.GetMoviesCallback {
            override fun onShowProgressDialog() {

            }

            override fun onHideProgressDialog() {

            }

            override fun onSuccess(data: List<Movie>) {
                movieListLive.apply {
                    value = data
                }
            }

            override fun onFinish() {

            }

            override fun onFailed(statusCode: Int, errorMessage: String?) {
                snackBarMessageRemote.value = errorMessage
            }
        })
    }
}
package id.co.gits.movies.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.gits.gitsutils.base.BaseFragment
import id.co.gits.gitsutils.data.model.Movie
import id.co.gits.gitsutils.extensions.navigatorImplicit
import id.co.gits.gitsutils.extensions.putArgs
import id.co.gits.gitsutils.extensions.showToast
import id.co.gits.gitsutils.extensions.verticalListStyle
import id.co.gits.movies.R
import id.co.gits.movies.databinding.MainFragmentBinding
import kotlinx.android.synthetic.main.main_fragment.*

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

class MainFragment : BaseFragment<MainViewModel>(), MainItemUserActionListener {

    lateinit var viewBinding: MainFragmentBinding

    lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = MainFragmentBinding.inflate(inflater, container, false).apply {
            viewModel = this@MainFragment.viewModel
        }
        return viewBinding.root
    }

    override fun onCreateObserver(viewModel: MainViewModel) {
        viewModel.apply {
            movieListLive.observe(this@MainFragment, Observer {
                // load movie list to view
                setupMovieList(it!!)
            })
        }
    }

    override fun setContentData() {
        viewModel.start()
    }

    override fun onCreateViewModel(): MainViewModel {
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        return viewModel
    }

    override fun setMessageType() = MESSAGE_TYPE_SNACK

    override fun onDestroyObserver(viewModel: MainViewModel) {
        viewModel.apply {
            movieListLive.removeObservers(this@MainFragment)
            eventGlobalMessage.removeObservers(this@MainFragment)
            eventShowProgress.removeObservers(this@MainFragment)
        }
    }

    override fun onMovieClicked(movie: Movie) {
        showToast(requireContext(), movie.original_title ?: getString(R.string.lorem_ipsum))
        navigatorImplicit(requireContext(),
                "id.co.gits.moviesdetail.MainDetailActivity")
    }

    private fun setupMovieList(movie: List<Movie>) {
        recycler_main.apply {
            verticalListStyle()
            adapter = MainAdapter(this@MainFragment, movie)
        }
    }

    companion object {
        fun newInstance() = MainFragment().putArgs { }
    }

}

package id.co.gits.movies.main

import id.co.gits.gitsutils.base.BaseUserActionListener
import id.co.gits.gitsutils.data.model.Movie

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

interface MainItemUserActionListener: BaseUserActionListener {
    fun onMovieClicked(movie: Movie)
}
package id.co.gits.moviesdetail

import id.co.gits.moviesdetail.databinding.MainDetailActivityBinding
import id.co.gits.gitsutils.base.BaseActivity

class MainDetailActivity : BaseActivity<MainDetailActivityBinding>() {

    override fun bindLayoutRes() = R.layout.main_detail_activity

    override fun bindToolbarId() = 0

    override fun bindFragmentInstance() = MainDetailFragment.newInstance(212)

    override fun bindFragmentContainerId() = R.id.frame_container

    override fun onStartWork() {

    }

}

package id.co.gits.gitsutils

import android.content.Context
import id.co.gits.gitsutils.data.source.GitsRepository
import id.co.gits.gitsutils.data.source.local.GitsLocalDataSource
import id.co.gits.gitsutils.data.source.remote.GitsRemoteDataSource

/**
 * Created by irfanirawansukirman on 26/01/18.
 */


object Injection {
    const val PREF_NAME = "-KEY"
    fun provideGitsRepository(context: Context): GitsRepository {
//       val localDatabase = GitsAppDatabase.getInstance(context)

        return GitsRepository.getInstance(GitsRemoteDataSource(context),
                GitsLocalDataSource(context,  context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)))
    }

}

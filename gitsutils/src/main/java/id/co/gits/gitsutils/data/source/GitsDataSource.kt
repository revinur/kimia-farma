package id.co.gits.gitsutils.data.source

import id.co.gits.gitsutils.base.BaseDataSource
import id.co.gits.gitsutils.data.model.Movie

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

interface GitsDataSource: BaseDataSource {

    fun getMovies(callback: GetMoviesCallback)

    fun saveMovie(movie: List<Movie>)

    interface GetMoviesCallback : BaseDataSource.GitsResponseCallback<List<Movie>>

    interface GetMoviesByIdCallback : BaseDataSource.GitsResponseCallback<Movie>

}
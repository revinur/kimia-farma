package id.co.gits.gitsutils.extensions

import android.content.Context
import id.co.gits.gitsutils.SharedPreferencesFactory
import id.co.gits.gitsutils.SharedPreferencesFactory.get
import id.co.gits.gitsutils.SharedPreferencesFactory.set

/**
 * Created by mochadwi on 12/26/2018.
 */

/**
 * {@link SharedPreferencesFactory#initPreferences()} // doesn't work
 * @see SharedPreferencesFactory#initPreferences.
 */
val Context.prefs
    get() = SharedPreferencesFactory.initPreferences(this)


//}
//
//fun Context.clearPrefs() {
////    prefs.edit().clear().apply()
//}
//
//// Ref: https://stackoverflow.com/a/49136401/3763032
fun Context.readPreference(key: String): Any? {
    val keys = prefs.all
    if (keys != null) {
        for (entry in keys) {
            if (entry.key == key) {
                return entry.value
            }
        }
    }
    return null
}
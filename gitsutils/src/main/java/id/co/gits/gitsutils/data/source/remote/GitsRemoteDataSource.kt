package id.co.gits.gitsutils.data.source.remote

import android.content.Context
import id.co.gits.gitsutils.data.model.Movie
import id.co.gits.gitsutils.data.source.GitsDataSource
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by irfanirawansukirman on 26/01/18.
 */
class GitsRemoteDataSource(context: Context) : GitsDataSource {

    private var compositeDisposable: CompositeDisposable? = null

    val gitsApiService = GitsApiService.getApiService(context)

    override fun getMovies(callback: GitsDataSource.GetMoviesCallback) {

    }

    override fun saveMovie(movie: List<Movie>) {
        // Tidak digunakan
    }

    override fun onClearDisposables() {
        compositeDisposable?.clear()
    }

    fun addSubscribe(disposable: Disposable) {
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
            compositeDisposable?.add(disposable)
        }
    }
}